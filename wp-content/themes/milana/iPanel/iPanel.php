<?php

    /*
     *    === Define the Path ===
    */
    defined('MILANA_IPANEL_PATH') ||
    
        define( 'MILANA_IPANEL_PATH' , get_template_directory() . '/iPanel/' );

    /*
     *    === Define the Version of iPanel ===
    */
    define( 'MILANA_IPANEL_VERSION' , '1.1' );    
    

    
    /*
     *    === Define the Classes Path ===
    */
    if ( defined('MILANA_IPANEL_PATH') ) {
        define( 'MILANA_IPANEL_CLASSES_PATH' , MILANA_IPANEL_PATH . 'classes/' );
    } else {

        define( 'MILANA_IPANEL_CLASSES_PATH' , get_template_directory() . '/iPanel/classes/' );
    }
    
    function milana_iPanelLoad(){
        require_once MILANA_IPANEL_CLASSES_PATH . 'ipanel.class.php';
		if( file_exists(MILANA_IPANEL_PATH . 'options.php') )
			require_once MILANA_IPANEL_PATH . 'options.php';
    }
    
    if ( defined('MILANA_IPANEL_PLUGIN_USAGE') ) {
        if ( MILANA_IPANEL_PLUGIN_USAGE === true ) {
            add_action('plugins_loaded', 'milana_iPanelLoad');
        } else {
            add_action('init', 'milana_iPanelLoad');
        }
    } else {
        add_action('init', 'milana_iPanelLoad');
    }