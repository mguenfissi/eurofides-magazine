# Changelog

    1.0.9
    # -> Fixed sticky totop scroller

    1.0.9
    # -> Fixed grid-slider-salt / grid-slider-salt2 safari display bug

    1.0.8
    # Fixed grid-slider-salt / grid-slider-salt2 dynamic grid overlapping images
    + Added grid-slider-salt / grid-slider-salt2 dynamic grid tag filter list

	1.0.7
	# Fixed template widgets media 'auto' settings
	# Fixed icon button height
	# Fixed uk-panel padding
	# Fixed modal blank close button

	1.0.6

	# Fixed sticky footer bug

	1.0.5
	# Fixed sticky footer

	1.0.4
	^ Updated UIkit theme according to UIkit 2.20.0
	# Fixed Article Navigation (J)

	1.0.3
	+ Added Footer position
	+ Added WooCommerce products per page option (WP)

	1.0.2
	# Fixed block padding for layout positions main-bottom and main-top
	# Fixed config.default.json
	# Changed height of HTML and body elements from % to vh to prevent site from jumping up when lightbox is opened
	# Fixed overlay-menu search for WordPress
	^ Updated accordion style
	+ Added theme classes tm-block-padding-bottom and tm-block-padding-top

	1.0.1
	# Added styles and images folder in templateDetails.xml

	1.0.0
	+ Initial Release



	* -> Security Fix
	# -> Bug Fix
	$ -> Language fix or change
	+ -> Addition
	^ -> Change
	- -> Removed
	! -> Note
