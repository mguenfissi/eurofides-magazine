/* Copyright (C) YOOtheme GmbH, http://www.gnu.org/licenses/gpl.html GNU/GPL */

jQuery(function($) {

    var config = $('html').data('config') || {};

    var initFunction

    // Social buttons
    $('article[data-permalink]').socialButtons(config);


    UIkit.on('afterready.uk.dom', function() {

        // Overlay Menu hover
        var overlayMenu = $('.tm-overlay-menu ul.uk-nav');

        if (overlayMenu.length) {

            overlayMenu.off('click.uikit.nav').on('mouseenter', '>li.uk-parent', function() {

                if (!this.classList.contains('uk-open')) {
                    overlayMenu.data('nav').open(this);
                }
            });
        }

    });


    // push footer to bottom of page
    UIkit.$win.on('load resize', UIkit.Utils.debounce((function(footer, fn, height, initTop) {
        
        footer  =  $('.tm-footer');

        if (!footer.length) return function(){};

        fn = function() {
            
            footer.css('margin-top', '');
            initTop = parseInt(footer.children().first().css('margin-top'), 10);
            
            if ((footer.offset().top <= window.innerHeight - footer.outerHeight())) {

                height = window.innerHeight - (footer.offset().top + footer.outerHeight()) + initTop;
                footer.css('margin-top', height);
            }

            return fn;
        };

        return fn();

    })(), 100));


    UIkit.$win.on('load', UIkit.Utils.debounce(function() {
        UIkit.$win.trigger('resize');
    }, 100));

});
