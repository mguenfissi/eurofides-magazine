<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Milana
 */

get_header();

// Count view
milana_setPostViews(get_the_ID());

?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content-single', 'eurofides' ); ?>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>