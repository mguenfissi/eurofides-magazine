<?php
/*
Plugin Name: Milana Theme Addons
Plugin URI: http://creanncy.com/
Description: 1-Click Demo Import and other extra theme features
Author: Creanncy
Version: 1.0
Author URI: http://creanncy.com/
Text Domain: milana-ta
License: General Public License
*/

// Load translated strings
add_action( 'init', 'milana_ta_load_textdomain' );

// load init
add_action( 'init', 'milana_ta_init' );

// after theme load
add_action('after_setup_theme', 'milana_ta_after_setup_theme');

// flush rewrite rules on deactivation
register_deactivation_hook( __FILE__, 'milana_ta_deactivation' );

function milana_ta_deactivation() {
	// Clear the permalinks to remove our post type's rules
	flush_rewrite_rules();
}

function milana_ta_load_textdomain() {
	load_plugin_textdomain( 'milana_ta_plugin', false, basename( dirname( __FILE__ ) ) . '/languages' );
}

// Init
function milana_ta_init() {
	global $pagenow;

	// Remove issues with prefetching adding extra views
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

	// Custom User social profiles
	function milana_add_to_author_profile( $contactmethods ) {

	    $social_array = array('facebook' => 'Facebook', 'twitter' => 'Twitter', 'vk' => 'Vkontakte', 'google-plus' => 'Google Plus', 'behance' => 'Behance', 'linkedin' => 'LinkedIn', 'pinterest' => 'Pinterest', 'deviantart' => 'DeviantArt', 'dribbble' => 'Dribbble',  'flickr' => 'Flickr', 'instagram' => 'Instagram', 'skype' => 'Skype', 'tumblr' => 'Tumblr', 'twitch' => 'Twitch', 'vimeo-square' => 'Vimeo', 'youtube' => 'Youtube');
	    
	    foreach ($social_array as $social_key => $social_value) {
	        # code...
	        $contactmethods[$social_key.'_profile'] = $social_value.' Profile URL';
	    }
	    
	    return $contactmethods;
	}
	add_filter( 'user_contactmethods', 'milana_add_to_author_profile', 10, 1);

	// 1-click demo importer
	if (( $pagenow !== 'admin-ajax.php' ) && (is_admin())) {
		require plugin_dir_path( __FILE__ ).'inc/oneclick-demo-import/init.php';
	}

	// Title backward compatibility
	if ( ! function_exists( '_wp_render_title_tag' ) ) {
		function milana_render_title() {

		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php

		}

		add_action( 'wp_head', 'milana_render_title' );
	}
}

// AFter theme load
function milana_ta_after_setup_theme() {
	// Allow shortcodes in widgets
	add_filter('widget_text', 'do_shortcode');
	add_filter('widget_milana_text', 'do_shortcode');
}